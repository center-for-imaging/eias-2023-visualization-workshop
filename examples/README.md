# Image data visualization case studies

A collection of examples involving scientific image processing and analysis in Python and visualization in Napari.

<table>
  <tr>
    <td style="vertical-align: center;">
      <a href="segmentation_3d.ipynb">n-D Image data visualization in Napari</a>
      <p>Introduction to the Napari viewer and segmentation of a 3D CT scan of a granular material sample using the watershed transform.</p>
    </td>
    <td>
      <a href="segmentation_3d.ipynb">
        <img src="../images/segmentation_fig.png" alt="segmentation_3d.ipynb" height="200px">
      </a>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: center;">
      <a href="tracking_2d.ipynb">Cell detection and tracking</a>
      <p>Detection, tracking, and visualization of dividing cell nuclei in a timeseries.</p>
    </td>
    <td>
      <a href="tracking_2d.ipynb">
        <img src="../images/tracking_fig.png" alt="tracking_2d.ipynb" height="200px">
      </a>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: center;">
      <a href="skeletonization.ipynb">Skeleton analysis of a drosophila trachea</a>
      <p>Analysis of the elongated branching structure of a drosophila trachea in a 3D confocal image using the <a href="https://skeleton-analysis.org/stable/index.html">Skan</a> package.</p>
    </td>
    <td>
      <a href="skeletonization.ipynb">
        <img src="../images/drosofig.png" alt="skeletonization.ipynb" height="200px">
      </a>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: center;">
      <a href="lungs_segmentation_ct.ipynb">Lungs convex hull detection</a>
      <p>Segmentation of the convex hull of the lungs of a mouse in a CT scan (3D).</p>
    </td>
    <td>
      <a href="lungs_segmentation_ct.ipynb">
        <img src="../images/lungs_fig.png" alt="lungs_segmentation_ct.ipynb" height="200px">
      </a>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: center;">
      <a href="crystallites_annotation.ipynb">Annotating triangular crystallites</a>
      <p>Interactive annotation of triangular crystallites and visualization of their orientation distribution.</p>
    </td>
    <td>
      <a href="crystallites_annotation.ipynb">
        <img src="../images/triangles_fig.png" alt="crystallites_annotation.ipynb" height="200px">
      </a>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: center;">
      <a href="image_correlation.ipynb">Digital image correlation</a>
      <p>Registration of two 3D images by the optical flow method using the <a href="https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/index.html">spam</a> package.</p>
    </td>
    <td>
      <a href="image_correlation.ipynb">
        <img src="../images/dic_fig.png" alt="image_correlation.ipynb" height="200px">
      </a>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: center;">
      <a href="nuclei_stardist.ipynb">Cell nuclei detection</a>
      <p>Deep learning based segmentation of cell nuclei in H&E images using <a href="https://github.com/stardist/stardist">StarDist</a>.</p>
    </td>
    <td>
      <a href="nuclei_stardist.ipynb">
        <img src="../images/stardist_fig.png" alt="nuclei_stardist.ipynb" height="200px">
      </a>
    </td>
  </tr>
</table>

------
[🔙 README.md](../README.md)