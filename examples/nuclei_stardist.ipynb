{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p align=\"center\">\n",
    "    <img src=\"https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/raw/main/images/stardist_fig.png\" height=\"350\" alt=\"skeletonization image\">\n",
    "</p>\n",
    "\n",
    "# Cell nuclei detection\n",
    "---\n",
    "\n",
    "[StarDist](https://github.com/stardist/stardist) is a deep-learning based Python library used for segmenting star-convex objects, such as cell nuclei, in 2D and 3D images. It is also available as plugins for [ImageJ](https://imagej.net/plugins/stardist), [Napari](https://github.com/stardist/stardist-napari), and [Qupath](https://qupath.readthedocs.io/en/0.3/docs/advanced/stardist.html).\n",
    "\n",
    "In this notebook, we will use StarDist to detect cell nuclei in an image extracted from the [DeepSlides](https://zenodo.org/record/1184621) public dataset of histopathology images."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setup\n",
    "\n",
    "Check that you have all the necessary packages installed, including `napari` and the `stardist-napari` plugin. If not, you can use the `!` symbol to install them directly from the Jupyter notebook (otherwise, you can use your terminal)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install stardist-napari"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import napari\n",
    "from stardist.models import StarDist2D"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get the data\n",
    "\n",
    "The image we'll use in this tutorial is available for download on [Zenodo](https://zenodo.org/record/8099852) (`deepslide.png`).\n",
    "\n",
    "In the cell below, we use a Python package called [pooch](https://pypi.org/project/pooch/) to automatically download the image from Zenodo into the **data** folder of this repository."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pooch\n",
    "from pathlib import Path\n",
    "\n",
    "data_path = Path('.').resolve().parent / 'data'\n",
    "fname = 'deepslide.png'\n",
    "\n",
    "pooch.retrieve(\n",
    "    url=\"https://zenodo.org/record/8099852/files/deepslide.png\",\n",
    "    known_hash=\"md5:67d2dac6f327e2d3749252d46799861a\",\n",
    "    path=data_path,\n",
    "    fname=fname,\n",
    "    progressbar=True,\n",
    ")\n",
    "\n",
    "print(f'Downloaded image {fname} into: {data_path}')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read the image\n",
    "\n",
    "We use the `imread` function from Scikit-image to read our PNG image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from skimage.io import imread\n",
    "\n",
    "image = imread(data_path / 'deepslide.png')\n",
    "\n",
    "print(f'Loaded image in an array of shape: {image.shape} and data type {image.dtype}')\n",
    "print(f'Intensity range: [{image.min()} - {image.max()}]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you run into troubles, don't hesitate to ask for help 🤚🏽."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load the image into Napari\n",
    "\n",
    "Let's open a viewer and load our image to have a look at it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "viewer = napari.Viewer()\n",
    "viewer.add_image(image, name=\"H&E (DeepSlides)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Intensity normalization\n",
    "\n",
    "Let's rescale our image to the range 0-1. By doing so, it is also converted to an array of data type `float`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from skimage.exposure import rescale_intensity\n",
    "\n",
    "image_normed = rescale_intensity(image, out_range=(0, 1))\n",
    "\n",
    "print(f'Intensity range: [{image_normed.min()} - {image_normed.max()}]')\n",
    "print(f'Array type: {image_normed.dtype}')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Instantiate a StarDist model\n",
    "\n",
    "The StarDist developers provide a few pre-trained models that may already be applied to suitable images.\n",
    "\n",
    "Here, we will use the *Versatile (H&E nuclei)* model that was trained on images from the MoNuSeg 2018 training data and the TNBC dataset from Naylor et al. (2018)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = StarDist2D.from_pretrained(\"2D_versatile_he\")\n",
    "\n",
    "model"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run the model\n",
    "\n",
    "We use the `predict_instances` method of the model to generate a segmenation mask (`labels`) and a representation of the cells as polygons (`polys`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels, polys = model.predict_instances(\n",
    "    image_normed,  # The image must be normalized!\n",
    "    axes=\"YXC\",\n",
    "    prob_thresh=0.5,  # Detection probability threshold\n",
    "    nms_thresh=0.1,  # Remove detections overlapping by more than this threshold\n",
    "    scale=1,  # Higher values are suitable for lower resolution data\n",
    "    return_labels=True,\n",
    ")\n",
    "\n",
    "# We also get detection probabilities:\n",
    "probabilities = list(polys[\"prob\"])\n",
    "\n",
    "n_detections = len(probabilities)\n",
    "\n",
    "print(f'{n_detections} cells detected.')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualization in Napari"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Create a custom color lookup table based on detection probabilities\n",
    "probas_incl_bg = np.zeros(n_detections + 1)  # Include a value for the background\n",
    "probas_incl_bg[1:] = probabilities\n",
    "\n",
    "colors = plt.cm.get_cmap('inferno')(probas_incl_bg)  # Convert probability values to colors\n",
    "colors[0, -1] = 0.0  # Make the background transparent (alpha channel = 0)\n",
    "colormap = dict(zip(np.arange(len(probas_incl_bg)), colors))\n",
    "\n",
    "labels_layer = viewer.add_labels(\n",
    "    labels, \n",
    "    name='Segmentation', \n",
    "    color=colormap,\n",
    "    properties={'probabilities': probas_incl_bg},\n",
    "    opacity=0.7\n",
    ")\n",
    "\n",
    "# In Napari, you can display some text in the top-left part of the window, for example:\n",
    "viewer.text_overlay.visible = True\n",
    "viewer.text_overlay.text = f'Number of detections: {n_detections}'"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The `stardist-napari` plugin\n",
    "\n",
    "In fact, a Napari [plugin for StarDist](https://github.com/stardist/stardist-napari) already exists. If you have installed it using `pip install stardist-napari`, you should be able to find it in the `Plugins` menu of Napari.\n",
    "\n",
    "You can check that the segmentation produced by the plugin is similar to what you got by running the model in this notebook!\n",
    "\n",
    "<p align=\"center\">\n",
    "    <img src=\"https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/raw/main/images/stardist_plugin_fig.png\" height=\"350\" alt=\"StarDist plugin\">\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p align=\"center\">\n",
    "    <a href=\"https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/blob/main/examples/README.md\">🔙 Back to case studies</a>\n",
    "</p>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
