{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p align=\"center\">\n",
    "    <img src=\"https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/raw/main/images/tracking_fig.png\" height=\"350\" alt=\"tracking image\">\n",
    "</p>\n",
    "\n",
    "# Cell detection and tracking\n",
    "---\n",
    "\n",
    "This notebook gives a practical introduction to blob detection and particle tracking in the context of a 2D cell lineage tracing challenge. It was adapted from an example from `napari.org` which you can check out here: [Single cell tracking with napari](https://napari.org/stable/tutorials/tracking/cell_tracking.html)."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setup\n",
    "\n",
    "First, check that you have all the necessary packages installed, including `napari`, `trackpy`, and `ipywidgets`. If not, you can use the `!` symbol to install them directly from the Jupyter notebook (otherwise, you can use your terminal)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install trackpy ipywidgets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import napari\n",
    "import trackpy as tp"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get the data\n",
    "\n",
    "The image we'll use in this tutorial is available for download on [Zenodo](https://zenodo.org/record/8099852) (`cell_tracking_2d.tif`). This image comes from the [cell tracking challenge](http://celltrackingchallenge.net/3d-datasets/).\n",
    "\n",
    "In the cell below, we use a Python package called [pooch](https://pypi.org/project/pooch/) to automatically download the image from Zenodo into the **data** folder of this repository."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pooch\n",
    "from pathlib import Path\n",
    "\n",
    "data_path = Path('.').resolve().parent / 'data'\n",
    "fname = 'cell_tracking_2d.tif'\n",
    "\n",
    "pooch.retrieve(\n",
    "    url=\"https://zenodo.org/record/8099852/files/cell_tracking_2d.tif\",\n",
    "    known_hash=\"md5:43f973785dcfbad38334fbf682a36d0f\",\n",
    "    path=data_path,\n",
    "    fname=fname,\n",
    "    progressbar=True,\n",
    ")\n",
    "\n",
    "print(f'Downloaded image {fname} into: {data_path}')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read the image\n",
    "\n",
    "We use the `imread` function from Scikit-image to read our TIF image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from skimage.io import imread\n",
    "\n",
    "image = imread(data_path / 'cell_tracking_2d.tif')\n",
    "\n",
    "print(f'Loaded image in an array of shape: {image.shape} and data type {image.dtype}')\n",
    "print(f'Intensity range: [{image.min()} - {image.max()}]')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you run into troubles, don't hesitate to ask for help 🤚🏽."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Blob detection\n",
    "---\n",
    "\n",
    "The goal of *Blob detection* is to detect the coordinates of bright, elliptical objects on a dark background - such as the cells in our image - at a given characteristic scale.\n",
    "\n",
    "To learn more about this topic, check out:\n",
    "\n",
    "- [Blob detection (Scikit-image)](https://scikit-image.org/docs/stable/auto_examples/features_detection/plot_blob.html)\n",
    "- [Detecting Blobs (First Principles of Computer Vision - Youtube)](https://www.youtube.com/watch?v=zItstOggP7M)\n",
    "\n",
    "Here, we detect our cell *blobs* by applying a series of `Laplacian of Gaussian` filters to the image at different scales. The scale is defined by the parameter `sigma` (the standard deviation of the Gaussian).\n",
    "\n",
    "We also use [Pandas](https://pandas.pydata.org/) to manipulate tabular data. To learn more about using Pandas for image data analysis, have a look at [this chapter](https://biapol.github.io/Image-data-science-with-Python-and-Napari-EPFL2022/day4a_Tabular_Data/Tabular_Data.html) from the course *Image data science with Python and Napari @EPFL*. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import IntProgress\n",
    "from IPython.display import display  # Import and use these to display a progress bar in Jupyter\n",
    "from skimage.exposure import rescale_intensity\n",
    "from skimage.feature import blob_log\n",
    "from skimage.transform import downscale_local_mean\n",
    "import pandas as pd\n",
    "\n",
    "# Initialize a Pandas DataFrame to collect tracks data.\n",
    "df = pd.DataFrame(columns=['y', 'x', 'sigma', 'frame'])\n",
    "\n",
    "# We downscale the image by this factor, using the local mean method.\n",
    "downscale_factor = 4\n",
    "\n",
    "# We rescale the intensity to the range (0, 1) to make it easier to select a threshold for the detection.\n",
    "image_normed = rescale_intensity(image, out_range=(0, 1))\n",
    "\n",
    "# Set up the progress bar\n",
    "pbar = IntProgress(min=0, max=len(image_normed))\n",
    "display(pbar)\n",
    "\n",
    "# Loop over the frames\n",
    "for frame_id, frame in enumerate(image_normed):\n",
    "    # We downscale the image; the cells are big enough and this will speed-up the workflow.\n",
    "    im = downscale_local_mean(frame, factors=tuple([downscale_factor]*2), )\n",
    "\n",
    "    # Tweaking the parameters for the Laplacian of Gaussian detector is necessary.\n",
    "    # Eventually good parameters can be found!\n",
    "    track_results = blob_log(im, \n",
    "        min_sigma=1.5, # Size of the smallest blob\n",
    "        max_sigma=6.0,  # Size of the biggest blob\n",
    "        threshold=0.1  # Lower = more detections\n",
    "    )\n",
    "    \n",
    "    # Since we downscaled the image, the detected coordinates must be rescaled\n",
    "    track_results[:, :3] *= downscale_factor\n",
    "\n",
    "    ys, xs, sigmas = track_results.T  # .T for transpose => the array shape goes from (N, 4) to (4, N)\n",
    "    df_frame = pd.DataFrame({\n",
    "        'y': ys,\n",
    "        'x': xs,\n",
    "        'sigma': sigmas,\n",
    "        'frame': frame_id\n",
    "    })\n",
    "\n",
    "    # Add the results of this frame to the total\n",
    "    df = pd.concat([df, df_frame])  \n",
    "\n",
    "    pbar.value += 1  # Increment the progress bar\n",
    "\n",
    "print(f'Total number of detections: {len(df)}')\n",
    "\n",
    "df.head() # `head` displays the first 5 elements of the data frame."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Besides the coordinates (`x`, `y`), the value of `sigma` indicates the size of the detected \"blob\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A quick plot of the number of detections overtime\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "vc = df['frame'].value_counts()  # Pandas way of counting the number of detections per frame\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(6, 4), dpi=120)\n",
    "ax.plot([vc[k] for k in range(len(image))])\n",
    "ax.set_xlim(0, len(image))\n",
    "ax.set_xlabel('Frame')\n",
    "ax.set_ylabel('Detections')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plotting the mean sigma value in every frame.. it looks like there are some peaks!\n",
    "# Is the cell division synchronized?\n",
    "mean_sigmas = df.groupby('frame').mean()['sigma'].values\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(6, 4), dpi=120)\n",
    "ax.plot(mean_sigmas)\n",
    "ax.set_xlim(0, len(image))\n",
    "ax.set_xlabel('Frame')\n",
    "ax.set_ylabel('Mean sigma')\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Particle tracking\n",
    "---\n",
    "\n",
    "In particle tracking, we want to compute a *linkage* between objects detected in consecutive frames. In Python, [Trackpy](http://soft-matter.github.io/trackpy/v0.6.1/) is a package for particle tracking in 2D, 3D, and higher dimensions.\n",
    "\n",
    "Here, we use the `link` function of Trackpy, which implements the [Crocker-Grier algorithm](http://dx.doi.org/10.1006/jcis.1996.0217) to compute the linkage between particles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the linkage using Trackpy.\n",
    "linkage_df = tp.link(df, search_range=30, memory=3)\n",
    "\n",
    "# This line is used to add the \"length\" column of the DataFrame.\n",
    "linkage_df = linkage_df.merge(\n",
    "    pd.DataFrame({'length': linkage_df['particle'].value_counts()}), \n",
    "    left_on='particle', right_index=True\n",
    ")\n",
    "\n",
    "# The DataFrame now has a `particle` column identifying the particle ID and a `length` column corresponding to the track length.\n",
    "linkage_df.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization in Napari\n",
    "---\n",
    "\n",
    "The `tracks` data should be a 2D array of shape (N, 4) representing four columns: the *track ID*, *frame ID*, *Y coordinate* and *X coordinate*. With the data in this order, a [`Tracks` layer](https://napari.org/stable/howtos/layers/tracks.html) can be added for visualization using `add_tracks`.\n",
    "\n",
    "We also add a separate [Points layer](https://napari.org/stable/howtos/layers/points.html) to visualize the results of the blob detection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract data for the Napari viz\n",
    "points = linkage_df[['frame', 'y', 'x']].values.astype(float)\n",
    "sigmas = linkage_df['sigma'].values.astype(float)\n",
    "lengths = linkage_df['length'].values.astype(float)\n",
    "tracks = linkage_df[['particle', 'frame', 'y', 'x']].values.astype(float)\n",
    "\n",
    "# Create the Napari Viewer setup. `view_image`` is a shortcut for `napari.Viewer().add_image()`.\n",
    "viewer = napari.view_image(image)\n",
    "\n",
    "viewer.add_points(\n",
    "    points,\n",
    "    name='Detections (LoG)',\n",
    "    face_color='sigma', \n",
    "    opacity=0.7, \n",
    "    edge_width=0.0, \n",
    "    size=sigmas+1,  # The size of the points can be parametrized\n",
    "    features={'sigma': sigmas}  # Used to colorize the points\n",
    ")\n",
    "\n",
    "viewer.add_tracks(\n",
    "    tracks, \n",
    "    name='Tracks (Trackpy)', \n",
    "    tail_width=4, \n",
    "    color_by='length', \n",
    "    properties={'length': lengths}  # Used to colorize the tracks\n",
    ")\n",
    "\n",
    "# A quick screenshot\n",
    "from napari.utils import nbscreenshot\n",
    "nbscreenshot(viewer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p align=\"center\">\n",
    "    <a href=\"https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/blob/main/examples/README.md\">🔙 Back to case studies</a>\n",
    "</p>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
