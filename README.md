

# Image Data Visualization with Python and Napari

<p align="center">
    <img src="https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/raw/main/images/segmentation_fig.png" height="350">
</p>

Napari is a multi-dimensional image viewer for Python. It is used to visualize scientific images and the data associated with them, such as segmentation masks, bounding boxes, and keypoints, for example.

With Napari, you can:

- Visualize timeseries, 2D, 3D, and multi-channel data.
- Create customized and interactive visualizations tailored to your needs.
- Set up your visualization in a Python script or a Jupyter notebook.
- Annotate data (draw masks, polygons, etc.).
- Use plugins from the [community](https://www.napari-hub.org/) or develop and share your own plugin.

## How to use this material

We recommend downloading this repository on your machine (for example, on your Desktop). You can download and unzip the repository or use `git` if you prefer.

<p align="center">
    <img src="https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/raw/main/images/zip_screenshot.png" height="350">
</p>

You can then `cd` into the repository folder from the command-line, activate your Python environment, and start Jupyter lab.

```
cd Desktop/eias-2023-visualization-workshop/
conda activate eias
jupyter lab
```

<p align="center">
    <img src="https://gitlab.epfl.ch/center-for-imaging/eias-2023-visualization-workshop/-/raw/main/images/jupyter_screenshot.png" height="350">
</p>

**💡 Tip:** to render the `README.md` markdown in Jupyter lab, you have to open the file, right-click in it and select *Show Markdown Preview*.

## [➡️ Case studies](examples/README.md)

Case studies in the form of Jupyter notebooks are in the **examples** folder. We recommend starting with the notebook [segmentation_3d.ipynb](examples/segmentation_3d.ipynb) as it goes over some basic Napari concepts.

## 🎓 Learning more
Check the links below to learn more about Napari.

<table>
  <tr>
    <td>
      <a href="https://napari.org/">
        <img src="https://github.com/MalloryWittwer/napari-workshop/blob/main/resources/napariOrg.svg?raw=true" alt="napari.org">
      </a>
    </td>
    <td>
      <a href="https://www.napari-hub.org/">
        <img src="https://github.com/MalloryWittwer/napari-workshop/blob/main/resources/napariHub.svg?raw=true" alt="napari-hub.org">
      </a>
    </td>
    <td>
      <a href="https://forum.image.sc/tag/napari">
        <img src="https://github.com/MalloryWittwer/napari-workshop/blob/main/resources/imageSC.svg?raw=true" alt="forum.image.sc">
      </a>
    </td>
    <td>
      <a href="https://github.com/napari/napari">
        <img src="https://github.com/MalloryWittwer/napari-workshop/blob/main/resources/gitHub.svg?raw=true" alt="github.com/napari">
      </a>
    </td>
  </tr>
</table>

----

🎉 Happy visualization!